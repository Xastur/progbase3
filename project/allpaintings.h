#ifndef ALLPAINTINGS_H
#define ALLPAINTINGS_H

#include <QDialog>
#include <QListWidget>
#include <QListWidgetItem>

#include "storage.h"
#include "sqlite_storage.h"
#include "paint.h"

//#include "paintingswindow.h"

namespace Ui {
class AllPaintings;
}

class AllPaintings : public QDialog
{
    Q_OBJECT

public:
    explicit AllPaintings(QWidget *parent = 0);
    ~AllPaintings();

signals:
    void sendPaintingToAddPW(Painting);

    void sendPaintingToPaintingsWindow(Painting);

private slots:
    void on_pushButton_cancel_clicked();

    void on_pushButton_ok_clicked();

    void on_listWidget_itemClicked(QListWidgetItem *item);

    void receiveStorage(Storage*);

    void updateListWidget();

private:
    Ui::AllPaintings *ui;
    QString file_path_;

    Storage * storage_ = nullptr;
};

#endif // ALLPAINTINGS_H
