#ifndef SIGNUPWINDOW_H
#define SIGNUPWINDOW_H

#include <QDialog>
#include <QMessageBox>

#include "user.h"
#include "storage.h"
#include "sqlite_storage.h"

namespace Ui {
class SignUpWindow;
}

class SignUpWindow : public QDialog
{
    Q_OBJECT

public:
    explicit SignUpWindow(QWidget *parent = 0);
    ~SignUpWindow();

private slots:
    void on_button_cancel_clicked();

    void on_button_ok_clicked();

    bool usernameExists();

private:
    Ui::SignUpWindow *ui;
    QSqlDatabase db_;
    QString file_path_;

};

#endif // SIGNUPWINDOW_H
