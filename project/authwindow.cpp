#include "authwindow.h"
#include "ui_authwindow.h"
#include "mainwindow.h"

AuthWindow::AuthWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AuthWindow)
{
    ui->setupUi(this);
}

AuthWindow::~AuthWindow()
{
    delete ui;
}

QString hashPasswordAuthWindow(QString const & pass)
{
   QByteArray pass_ba = pass.toUtf8();
   QByteArray hash_ba = QCryptographicHash::hash(pass_ba, QCryptographicHash::Md5);
   QString pass_hash = QString(hash_ba.toHex());
   return pass_hash;
}

void AuthWindow::on_button_ok_clicked()
{
    dir_name_ = "/home/dima/progbase3/project/data/sqlite/data.sqlite";
    SqliteStorage * sql_storage = new SqliteStorage(dir_name_);
    Storage * storage_ = sql_storage;
    if (!storage_->open())
    {
        qDebug() << "Can't open storage_";
        QMessageBox::information(
            this,
            "Error",
            "Can not open storage");
        return;
    }

    string usrname = ui->lineEdit_username->text().toStdString();
    string password =  ui->lineEdit_password->text().toStdString();
    optional<User> user_opt = storage_->getUserAuth(usrname, password);

    if (user_opt != nullopt)
    {
        main_window = new MainWindow;
        User user_ = user_opt.value();
        User user;
        user.id = user_.id;
        user.password_hash = hashPasswordAuthWindow(ui->lineEdit_password->text()).toStdString();
        user.username = ui->lineEdit_username->text().toStdString();
        connect(this, SIGNAL(sendUser(User)), main_window, SLOT(receiveUser(User)));
        emit(sendUser(user));
        main_window->loadMuseum(QString::fromStdString(dir_name_));
        storage_->close();
        delete storage_;
        this->close();
        main_window->show();
        return;
    }
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(
        this,
        "Wrong username or password",
        "Retry?",
        QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes)
    {
        ui->lineEdit_username->setText("");
        ui->lineEdit_password->setText("");
    }
    else
        this->close();
}

void AuthWindow::on_button_cancel_clicked()
{
    this->close();
}

void AuthWindow::on_pushButton_clicked()
{
    SignUpWindow * s_w = new SignUpWindow(this);
    s_w->exec();
}
