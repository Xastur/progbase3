#pragma once

#include <QMetaType>
#include <string>

using namespace std;

struct Museum
{
    int id_ = 0;
    string name_;
    string city_;
    int year_;
    string picture_path;
};

Q_DECLARE_METATYPE(Museum)
