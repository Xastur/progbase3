#ifndef EDITWINDOW_H
#define EDITWINDOW_H

#include <QDialog>
#include <QMainWindow>
#include <QListWidget>
#include <QFileDialog>
#include <QMessageBox>

#include "museum.h"

namespace Ui {
class EditWindow;
}

class EditWindow : public QDialog
{
    Q_OBJECT

public:
    explicit EditWindow(QWidget *parent = 0);
    ~EditWindow();

signals:
    void sendUpdMuseum(Museum);

private slots:
    void on_OK_bt_clicked();

    void on_Cancel_bt_clicked();

    void receiveMuseumToEdit(QListWidgetItem*);

    void on_pushButton_pic_path_clicked();

private:
    Ui::EditWindow *ui;
};

#endif // EDITWINDOW_H
