#include "editwindow.h"
#include "ui_editwindow.h"

EditWindow::EditWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditWindow)
{
    ui->setupUi(this);
    QIcon icon("/home/dima/progbase3/project/data/pictures/arrow.png");
    ui->pushButton_pic_path->setIcon(icon);
}

EditWindow::~EditWindow()
{
    delete ui;
}

void EditWindow::on_OK_bt_clicked()
{
    if(ui->NewCity_lineEdit->text().length() == 0 || ui->NewName_lineEdit_2->text().length() == 0)
    {
        QMessageBox::information(
            this,
            "Error",
            "City or name line is empty");
        return;
    }

    Museum museum;

    museum.city_ = ui->NewCity_lineEdit->text().toStdString();
    museum.name_ = ui->NewName_lineEdit_2->text().toStdString();
    museum.year_ = ui->spinBox_new_year->text().toInt();
    museum.picture_path = ui->lineEdit_pic_path->text().toStdString();
    emit sendUpdMuseum(museum);

    EditWindow::close();
}

void EditWindow::on_Cancel_bt_clicked()
{
    EditWindow::close();
}

void EditWindow::receiveMuseumToEdit(QListWidgetItem * item)
{
    QVariant var = item->data(Qt::UserRole);
    Museum mus = var.value<Museum>();

    ui->NewName_lineEdit_2->setText(QString::fromStdString(mus.name_));
    ui->NewCity_lineEdit->setText(QString::fromStdString(mus.city_));
    ui->spinBox_new_year->setValue(mus.year_);
    ui->lineEdit_pic_path->setText(QString::fromStdString(mus.picture_path));
}

void EditWindow::on_pushButton_pic_path_clicked()
{
    QString filePath = QFileDialog::getOpenFileName(
                this,              // parent
                "Dialog Caption",  // caption
                "../project/data/pictures",                // directory to start with
                "Image Files (*.png *.jpg *.bmp)");  // file name filter
    ui->lineEdit_pic_path->setText(filePath);
}
