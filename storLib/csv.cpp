#include "csv.h"

CsvTable Csv::createTableFromString(const string & csv_str)
{
    CsvTable table;
    CsvRow row;
    const char * csvString = csv_str.c_str();
    char buf[100];
    int bufX = 0;

    while (true)
    {
        if (*csvString == ',')
        {
            buf[bufX] = '\0';
            bufX = 0;
            row.push_back(buf);
        }
        else if (*csvString == '\n')
        {
            if (bufX != 0)
            {
                buf[bufX] = '\0';
                bufX = 0;
                row.push_back(buf);
                table.push_back(row);
            }
            row.clear();
        }
        else if (*csvString == '\0')
        {
            if (bufX == 0)
            {
                break;
            }

            buf[bufX] = '\0';
            row.push_back(buf);
            table.push_back(row);
            break;
        }
        else
        {
            buf[bufX++] = *csvString;
        }

        csvString += 1;
    }
    return table;
}

string Csv::createStringFromTable(const CsvTable &csv_table)
{
    string csv_text;

    for (size_t i = 0; i < csv_table.size(); i++)
    {
        CsvRow row = csv_table.at(i);
        for (size_t j = 0; j < row.size(); j++)
        {
            csv_text += row.at(j);

            if (j != row.size() - 1)
            {
                csv_text += ',';
            }
        }
        if (i != csv_table.size() - 1)
        {
            csv_text += '\n';
        }
    }
    return csv_text;
}
