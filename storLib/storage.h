#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <qsqlquery.h>

#include "optional.h"
#include "museum.h"
#include "csv.h"
#include "user.h"
#include "paint.h"

using std::string;
using std::vector;

class Storage
{
public:
    virtual ~Storage() = default;

    virtual bool open() = 0;
    virtual bool close() = 0;
    virtual int  size() = 0;
    virtual int size(int user_id) = 0;

    //museums
    virtual vector<Museum> getAllUserMuseums(int user_id) = 0;
    virtual vector<Museum> getAllMuseums(void) = 0;
    virtual optional<Museum> getMuseumById(int museum_id) = 0;
    virtual bool updateMuseum(const Museum & museum) = 0;
    virtual bool removeMuseum(int museum_id) = 0;
    virtual int insertMuseum(const Museum & museum) = 0;
    virtual int insertMuseum(const Museum & museum, int user_id) = 0;
    virtual Museum getMuseumFromQuery(const QSqlQuery & query) = 0;

    //paintings
    virtual int insertPainting(const Painting & painting, int user_id) = 0;
    virtual bool updatePainting(const Painting & painting) = 0;
    virtual int removePainting(int painting_id) = 0;
    virtual vector<Painting> getAllUserPaintings(int user_id) = 0;

    // users
    virtual optional<User> getUserAuth(string & username, string & password) = 0;

    // links
    virtual vector<Painting> getAllMuseumPaintings(int museum_id, int user_id) = 0;
    virtual vector<Painting> getMuseumPaintings(int museum_id, int user_id, int page_size, int page_number) = 0;
    virtual bool insertMuseumPainting(int musuem_id, int painting_id, int user_id) = 0;
    virtual bool removeMuseumsPainting(int museum_id, int painting_id, int user_id) = 0;
    virtual bool removeByMuseumId(int museum_id, int user_id) = 0;
    virtual bool removeByPaintingId(int paintig_id, int user_id) = 0;

    virtual int numberOfElements(int user_id) = 0;
    virtual int numberOfPaintings(int museum_id) = 0;
    virtual vector<Museum> search(QString search_str, int user_id, int page_size, int page_number) = 0;
    virtual int searchedNumber(QString search_str, int user_id) = 0;
};
