#include "mainwindow.h"
#include "authwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QPixmap pix("/home/dima/progbase3/project/data/pictures/white.png");
    ui->label_pic->setPixmap(pix);

    QPixmap pix2("/home/dima/progbase3/project/data/pictures/x.png");
    ui->pushButton_x->setIcon(pix2);
}

void MainWindow::loadMuseum(QString file_path)
{
    file_name = file_path;
    SqliteStorage * sql_storage = new SqliteStorage(file_name.toStdString());
    storage_ = sql_storage;
    if(!storage_->open())
    {
        cerr << "Can't open storage: " << file_name.toStdString();
        return;
    }

    int number_of_elements = storage_->numberOfElements(user_.id);
    int number_of_pages = number_of_elements / page_size_;
    if ((number_of_elements % page_size_) != 0 || number_of_elements == 0)
        number_of_pages += 1;
    last_page_ = number_of_pages;
    ui->label_page->setText(QString::fromStdString(to_string(page_number_)) + "/" + QString::fromStdString(to_string(last_page_)));

    updateListWidget();

    ui->listWidget->setEnabled(true);
    ui->pushButton_4->setEnabled(true);
    ui->Name_label->setEnabled(true);
    ui->Location_label->setEnabled(true);
    ui->Year_label->setEnabled(true);
    ui->namelabel->setEnabled(true);
    ui->locationlabel->setEnabled(true);
    ui->yearlabel->setEnabled(true);
    ui->SelectedMus_label->setEnabled(true);
}

MainWindow::~MainWindow()
{
    delete storage_;
    delete ui;
}

void MainWindow::on_pushButton_4_clicked() //add
{
    add_window = new AddWindow(this);
    add_window->show();

    connect(add_window, SIGNAL(sendMuseum(Museum)), this, SLOT(receiveMuseum(Museum)));
}

vector<int> allPaintingsId(int museum_id)
{
    vector<int> paintings_id;
    QSqlQuery query;
    query.prepare("SELECT * FROM links WHERE museum_id = :museum_id");
    query.bindValue(":museum_id", museum_id);
    if (!query.exec())
    {
        qDebug() << query.lastError();
        return paintings_id;
    }
    while (query.next())
    {
        paintings_id.push_back(query.value("painting_id").toInt());
    }
    return paintings_id;
}

void MainWindow::on_pushButton_5_clicked() //delete
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(
        this,
        "On delete",
        "Are you sure?",
        QMessageBox::Yes|QMessageBox::No);
    if (reply != QMessageBox::Yes)
    {
        return;
    }

    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();

    foreach (QListWidgetItem * item, items)
    {
        items = ui->listWidget->selectedItems();
        QVariant var = item->data(Qt::UserRole);
        Museum mus = var.value<Museum>();
        int ind = mus.id_;

        if(!storage_->removeMuseum(ind))
        {
            return;
        }
        else
        {
            vector<int> vec = allPaintingsId(ind);
            if (vec.size() > 0)
            {
                if(!storage_->removeByMuseumId(ind, user_.id))
                    qDebug() << "Can't remove connection in mainwindow.cpp";

                connect(this, SIGNAL(removePaintingsOfMuseum(vector<int>, int)), paintings_window, SLOT(receievePaintingsToDelete(vector<int>, int)));
                emit(removePaintingsOfMuseum(vec, ind));
            }
        }
    }

    int number_of_elements = storage_->numberOfElements(user_.id);
    if ((number_of_elements % page_size_) == 0)
    {
        if (page_number_ == last_page_ && last_page_ != 1)
            page_number_ -= 1;
        if (last_page_ != 1)
            last_page_ -= 1;
        ui->label_page->setText(QString::fromStdString(to_string(page_number_)) + "/" + QString::fromStdString(to_string(last_page_)));

    }

    if (ui->lineEdit_search->text().length() != 0)
        on_lineEdit_search_textChanged(ui->lineEdit_search->text());
    else
        updateListWidget();

    ui->pushButton_5->setEnabled(false);
    ui->edit_bt->setEnabled(false);
    ui->pushButton_paintings->setEnabled(false);
    ui->label_pic->setEnabled(false);

    ui->namelabel->setText("-");
    ui->locationlabel->setText("-");
    ui->yearlabel->setText("-");

    QPixmap pix("/home/dima/progbase3/project/data/pictures/white.png");
    ui->label_pic->setPixmap(pix);
}

void MainWindow::on_edit_bt_clicked() //edit
{
    edit_window = new EditWindow(this);
    edit_window->show();

    connect(this, SIGNAL(sendMuseumToEditForm(QListWidgetItem*)), edit_window, SLOT(receiveMuseumToEdit(QListWidgetItem*)));
    connect(edit_window, SIGNAL(sendUpdMuseum(Museum)), this, SLOT(receiveUpdMuseum(Museum)));

    QListWidgetItem * item = ui->listWidget->selectedItems().at(0);
    emit(sendMuseumToEditForm(item));

    ui->edit_bt->setEnabled(false);
    ui->pushButton_5->setEnabled(false);
    ui->pushButton_paintings->setEnabled(false);
}

void MainWindow::on_listWidget_itemClicked(QListWidgetItem * museum)
{
    ui->pushButton_5->setEnabled(true);
    ui->edit_bt->setEnabled(true);
    ui->pushButton_paintings->setEnabled(true);
    ui->label_pic->setEnabled(true);

    QVariant var = museum->data(Qt::UserRole);
    Museum mus = var.value<Museum>();

    string year = to_string(mus.year_);
    ui->namelabel->setText(QString::fromStdString(mus.name_));
    ui->locationlabel->setText(QString::fromStdString(mus.city_));
    ui->yearlabel->setText(QString::fromStdString(year));

    if (QFile::exists(QString::fromStdString(mus.picture_path)))
    {
        QPixmap pix(QString::fromStdString(mus.picture_path));
        ui->label_pic->setPixmap(pix);
    }
    else
    {
        QPixmap pix("/home/dima/progbase3/project/data/pictures/white.png");
        ui->label_pic->setPixmap(pix);
    }
}

void MainWindow::receiveMuseum(Museum museum)
{
    storage_->insertMuseum(museum, user_.id);

    int number_of_elements = storage_->numberOfElements(user_.id);
    if ((number_of_elements % page_size_) == 1 && number_of_elements != 1)
    {
        last_page_ += 1;
        ui->label_page->setText(QString::fromStdString(to_string(page_number_)) + "/" + QString::fromStdString(to_string(last_page_)));
    }

    if (ui->lineEdit_search->text().length() != 0)
        on_lineEdit_search_textChanged(ui->lineEdit_search->text());
    else
        updateListWidget();
}

void MainWindow::receiveUpdMuseum(Museum museum)
{
    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();
    QListWidgetItem * item = items.at(0);
    QVariant var = item->data(Qt::UserRole);
    Museum mus = var.value<Museum>();
    museum.id_ = mus.id_;

    if(!storage_->updateMuseum(museum))
    {
        return;
    }

    if (ui->lineEdit_search->text().length() != 0)
        on_lineEdit_search_textChanged(ui->lineEdit_search->text());
    else
        updateListWidget();}

void MainWindow::receiveUser(User user)
{
    user_.id = user.id;
    user_.password_hash = user.password_hash;
    user_.username = user.username;
}

void MainWindow::on_pushButton_paintings_clicked()
{
    paintings_window = new PaintingsWindow(this);
    paintings_window->show();

    connect(this, SIGNAL(sendUserIdToPF(int)), paintings_window, SLOT(receiveUserId(int)));
    connect(this, SIGNAL(sendMuseumIdToPF(int)), paintings_window, SLOT(receiveMuseumId(int)));

    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();
    QListWidgetItem * item = items.at(0);
    QVariant var = item->data(Qt::UserRole);
    Museum mus = var.value<Museum>();

    emit(sendUserIdToPF(user_.id));
    emit(sendMuseumIdToPF(mus.id_));

    paintings_window->load(file_name);
}

bool mystrcmp(QString a, QString b)
{
    int len = 0;
    if (a.length() < b.length())
        len = a.length();
    else
        len = b.length();

    for (int i = 0; i < len; i++)
    {
        if (a.at(i) != b.at(i))
            return false;
    }
    return true;
}

void MainWindow::on_lineEdit_search_textChanged(const QString & arg1)
{
    ui->pushButton_5->setEnabled(false);
    ui->edit_bt->setEnabled(false);
    ui->pushButton_paintings->setEnabled(false);

    ui->listWidget->clear();

    int number_of_elements = storage_->searchedNumber(ui->lineEdit_search->text(), user_.id);
    int number_of_pages = number_of_elements / page_size_;
    if ((number_of_elements % page_size_) != 0 || number_of_elements == 0)
        number_of_pages += 1;
    last_page_ = number_of_pages;
    if (last_page_ < page_number_)
            page_number_ = last_page_;

    vector<Museum> vec_mus = storage_->search(arg1, user_.id, page_size_, page_number_);
    ui->label_page->setText(QString::fromStdString(to_string(page_number_)) + "/" + QString::fromStdString(to_string(last_page_)));

    int num = (page_number_ - 1) * page_size_;
    for (int i = 0; i < vec_mus.size(); i++)
    {
        QListWidget * ListWidget = ui->listWidget;
        QListWidgetItem * qMuseumListItem = new QListWidgetItem();

        QVariant qVariant;
        qVariant.setValue(vec_mus.at(i));

        QString name;
        name = QString::fromStdString(to_string(num + 1)) + ". " + QString::fromStdString(vec_mus.at(i).name_);
        qMuseumListItem->setText(name);
        qMuseumListItem->setData(Qt::UserRole, qVariant);

        ListWidget->addItem(qMuseumListItem);
        num++;
    }
}

void MainWindow::on_pushButton_x_clicked()
{
    ui->pushButton_5->setEnabled(false);
    ui->edit_bt->setEnabled(false);
    ui->pushButton_paintings->setEnabled(false);
    ui->lineEdit_search->clear();
}

void MainWindow::on_pushButton_prev_clicked()
{
    ui->pushButton_5->setEnabled(false);
    ui->edit_bt->setEnabled(false);
    ui->pushButton_paintings->setEnabled(false);
    if (storage_->numberOfElements(user_.id) == 0)
        return;
    if (page_number_ != 1)
        page_number_ -= 1;
    else
        return;
    ui->label_page->setText(QString::fromStdString(to_string(page_number_)) + "/" + QString::fromStdString(to_string(last_page_)));
    if (ui->lineEdit_search->text().length() != 0)
    {
        on_lineEdit_search_textChanged(ui->lineEdit_search->text());
    }
    else
    {
        updateListWidget();
    }
}

void MainWindow::on_pushButton_next_clicked()
{
    ui->pushButton_5->setEnabled(false);
    ui->edit_bt->setEnabled(false);
    ui->pushButton_paintings->setEnabled(false);
    if (storage_->numberOfElements(user_.id) == 0)
        return;
    if (page_number_ != last_page_)
        page_number_ += 1;
    else
        return;
    ui->label_page->setText(QString::fromStdString(to_string(page_number_)) + "/" + QString::fromStdString(to_string(last_page_)));
    if (ui->lineEdit_search->text().length() != 0)
    {
        on_lineEdit_search_textChanged(ui->lineEdit_search->text());
    }
    else
    {
        updateListWidget();
    }
}

void MainWindow::updateListWidget()
{
    QSqlQuery query;
    query.prepare("SELECT * FROM museums WHERE user_id = :user_id LIMIT :page_size OFFSET :skipped_items");
    query.bindValue(":page_size", page_size_);
    query.bindValue(":skipped_items", (page_number_ - 1) * page_size_);
    query.bindValue(":user_id", user_.id);
    if (!query.exec())
    {
        qDebug() << "ERROR:" << query.lastError();
        return;
    }
    ui->listWidget->clear();
    int i = (page_number_ - 1) * page_size_;
    while(query.next())
    {
        Museum museum = storage_->getMuseumFromQuery(query);

        QListWidget * ListWidget = ui->listWidget;
        QListWidgetItem * qMuseumListItem = new QListWidgetItem();

        QVariant qVariant;
        qVariant.setValue(museum);

        QString name;
        name = QString::fromStdString(to_string(i + 1)) + ". " + QString::fromStdString(museum.name_);
        qMuseumListItem->setText(name);
        qMuseumListItem->setData(Qt::UserRole, qVariant);

        ListWidget->addItem(qMuseumListItem);
        i++;
    }
}

void MainWindow::on_actionCU_triggered()
{
    MainWindow::close();
    AuthWindow a;
    a.exec();
}

void MainWindow::on_actionExit_triggered()
{
    MainWindow::close();
}

void MainWindow::on_actionImport_triggered()
{
    QString filePath = QFileDialog::getOpenFileName(
                this,              // parent
                "Dialog Caption",  // caption
                "../project/data/csv",                // directory to start with
                "Csv (*.csv)");  // file name filter

    if (filePath.length() == 0)
    {
        return;
    }
    csv_storage_ = new CsvStorage(filePath.toStdString());
    if(!csv_storage_->open())
    {
        qDebug() << "can not open storage";
        QMessageBox::information(
            this,
            "Error",
            "Can not open storage");
        delete csv_storage_;
        return;
    }

    vector<Museum> museums = csv_storage_->getAllMuseums();
    for (size_t i = 0; i < museums.size(); i++)
    {
        receiveMuseum(museums.at(i));
    }
    updateListWidget();
    delete csv_storage_;
}

void MainWindow::on_actionExport_triggered()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    QString current_dir = QDir::currentPath();
    QString default_name = "new";
    QString folder_path = dialog.getSaveFileName(
        this,
        "Select Folder To Save CSV File",
        current_dir + "/" + default_name + ".csv",
        "Folders");

    if (folder_path.length() == 0)
    {
        return;
    }
    csv_storage_ = new CsvStorage(folder_path.toStdString());
    vector<Museum> vec_mus = storage_->getAllUserMuseums(user_.id);
    for (size_t i = 0; i < vec_mus.size(); i++)
    {
        csv_storage_->insertMuseum(vec_mus.at(i));
    }
    if (!csv_storage_->close())
    {
        qDebug() << "Can not close csv_storage";
        QMessageBox::information(
            this,
            "Error",
            "Can not close storage");
    }
    delete csv_storage_;
}
