#ifndef PAINTINGSWINDOW_H
#define PAINTINGSWINDOW_H

#include <QDialog>
#include <QListWidget>
#include <QListWidgetItem>
#include <QMessageBox>

#include "paint.h"
#include "storage.h"
#include "sqlite_storage.h"
#include "addpaintingwindow.h"
#include "editpaintingwindow.h"
#include "allpaintings.h"
#include "csv_storage.h"

namespace Ui {
class PaintingsWindow;
}

class PaintingsWindow : public QDialog
{
    Q_OBJECT

public:
    explicit PaintingsWindow(QWidget *parent = 0);
    ~PaintingsWindow();
    void load(QString file_name);

signals:
    void sendPaintingToEditForm(QListWidgetItem*);

    void sendStorageToAPW(Storage*);

private slots:
    void on_pushButton_add_clicked();

    void on_pushButton_close_clicked();

    void on_pushButton_remove_clicked();

    void on_pushButton_edit_clicked();

    void receiveUserId(int user_id);

    void receiveMuseumId(int museum_id);

    void receivePainting(Painting painting);

    void receiveUpdPainting(Painting painting);

    void on_listWidget_itemClicked(QListWidgetItem * painting);

    void receievePaintingsToDelete(vector<int>, int museum_id);

    void receivePaintingFromAddPW(Painting);

    bool isCreatorOfPainting(int museum_id, int painting_id);

    void on_pushButton_import_clicked();

    void on_pushButton_export_clicked();

    void updateListWidget();

    void on_pushButton_prev_clicked();

    void on_pushButton_next_clicked();

private:
    Ui::PaintingsWindow *ui;

    Storage * storage_ = nullptr;
    CsvStorage * csv_storage_;
    AddPaintingWindow * add_window;
    EditPaintingWindow * edit_paintings_window;
    AllPaintings * all_paintings_window_;
    QString file_name_;
    int user_id_;
    int museum_id_;
    bool sort_name_;

    int page_number_ = 1;
    int last_page_ = 0;
    int page_size_ = 5;
};

#endif // PAINTINGSWINDOW_H
