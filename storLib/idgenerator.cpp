#include "idgenerator.h"

int idGenerator::id_ = 0;

int idGenerator::id_get()
{
    id_ += 1;
    return id_;
}

void idGenerator::reset()
{
    id_ = 0;
}
