#include "ui_addpaintingwindow.h"
#include "allpaintings.h"
#include "addpaintingwindow.h"

AddPaintingWindow::AddPaintingWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddPaintingWindow)
{
    ui->setupUi(this);
    QIcon icon("/home/dima/progbase3/project/data/pictures/arrow.png");
    ui->pushButton_pic_path->setIcon(icon);
}

AddPaintingWindow::~AddPaintingWindow()
{
    delete ui;
}

void AddPaintingWindow::on_Cancel_bt_clicked()
{
    AddPaintingWindow::close();
}

void AddPaintingWindow::on_OK_bt_clicked()
{
    if (ui->Author_lineEdit->text().length() == 0 || ui->Painting_lineEdit->text().length() == 0)
    {
        QMessageBox::information(
            this,
            "Error",
            "Author or name line is empty");
        return;
    }
    Painting painting;
    painting.author_ = ui->Author_lineEdit->text().toStdString();
    painting.painting_ = ui->Painting_lineEdit->text().toStdString();
    painting.year_ = ui->spinBox_year->text().toInt();

    if (ui->lineEdit_pic_path->text().length() != 0)
        painting.picture_path_ = ui->lineEdit_pic_path->text().toStdString();
    else
        painting.picture_path_ = "-";

    emit sendPainting(painting);

    AddPaintingWindow::close();
}

void AddPaintingWindow::on_pushButton_pic_path_clicked()
{
    QString filePath = QFileDialog::getOpenFileName(
                this,              // parent
                "Dialog Caption",  // caption
                "../project/data/pictures",                // directory to start with
                "Image Files (*.png *.jpg *.bmp)");  // file name filter
    ui->lineEdit_pic_path->setText(filePath);
}

void AddPaintingWindow::on_pushButton_ex_painting_clicked()
{
    all_paintings_ = new AllPaintings(this);
    all_paintings_->show();

    connect(this, SIGNAL(sendStorageToAP(Storage*)), all_paintings_, SLOT(receiveStorage(Storage*)));
    emit sendStorageToAP(storage_);

    connect(all_paintings_, SIGNAL(sendPaintingToAddPW(Painting)), this, SLOT(receivePaintingFromAllPaintings(Painting)));
    AddPaintingWindow::close();
}

void AddPaintingWindow::receiveStorage(Storage * storage)
{
    storage_ = storage;
    qDebug() << "Receive storage in addpaintingwindow.cpp";
}

void AddPaintingWindow::receivePaintingFromAllPaintings(Painting p)
{
    ui->Painting_lineEdit->setText(QString::fromStdString(p.painting_));
    ui->Author_lineEdit->setText(QString::fromStdString(p.author_));
    ui->spinBox_year->setValue(p.year_);

    qDebug() << "In receivePaintingFromAllPaintings";

    emit(sendExPaintingToPaintingWindow(p));
}
