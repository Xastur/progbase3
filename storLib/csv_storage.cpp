#include "csv_storage.h"
#include "idgenerator.h"

using namespace std;

Museum CsvStorage::rowToMuseum(const CsvRow & row)
{
    Museum museum;

    if (row.at(0) == "0")
        museum.id_ = idGenerator::id_get();
    else
        museum.id_ = atoi(row.at(0).c_str());

    museum.name_ = row.at(1);
    museum.city_ = row.at(2);
    museum.year_ = atoi(row.at(3).c_str());
    museum.picture_path = row.at(4);

    return museum;
}

CsvRow CsvStorage::MuseumToRow(const Museum & mus)
{
    CsvRow row;

    row.push_back(to_string(mus.id_));
    row.push_back(mus.name_);
    row.push_back(mus.city_);
    row.push_back(to_string(mus.year_));
    if (mus.picture_path.length() == 0)
        row.push_back("-");
    else
        row.push_back(mus.picture_path);

    return row;
}

CsvRow CsvStorage::PaintingToRow(const Painting & painting)
{
    CsvRow row;

    row.push_back(to_string(painting.id_));
    row.push_back(painting.painting_);
    row.push_back(painting.author_);
    row.push_back(to_string(painting.year_));
    if (painting.picture_path_.length() != 0)
        row.push_back(painting.picture_path_);
    else
        row.push_back("-");

    return row;
}

int CsvStorage::getSizeFromFile(string file_name)
{
    ifstream file;
    file.open(file_name, ios::in);
    if (file.fail())
    {
        return -1; //error
    }
    string text;
    int size = 0;
    while(getline(file, text))
    {
        size += 1;
    }
    return size;
}

bool CsvStorage::open()
{
    string file_name = this->dir_name_;
    ifstream file;
    file.open(file_name, ios::in);
    if (file.fail())
    {
        return false;
    }
    string text_str;
    string row_str;
    while(getline(file, row_str))
    {
        text_str += row_str + '\n';
    }
    file.close();
    CsvTable table = Csv::createTableFromString(text_str);
    for (CsvRow & row : table)
    {
        Museum mus;
        mus = rowToMuseum(row);
        mus.id_ = idGenerator::id_get();
        this->museums_.push_back(mus);
    }

    return true;
}

Painting CsvStorage::rowToPaint(const CsvRow & row)
{
    Painting paint;

    if (row.at(0) == "0")
        paint.id_ = idGenerator::id_get();
    else
        paint.id_ = atoi(row.at(0).c_str());

    paint.painting_ = row.at(1);
    paint.author_ = row.at(2);
    paint.year_ = atoi(row.at(3).c_str());
    paint.picture_path_ = row.at(4);

    return paint;
}

bool CsvStorage::openExtra()
{
    string file_name = this->dir_name_;
    ifstream file;
    file.open(file_name, ios::in);
    if (file.fail())
    {
        return false;
    }
    string text_str;
    string row_str;
    while(getline(file, row_str))
    {
        text_str += row_str + '\n';
    }
    file.close();
    CsvTable table = Csv::createTableFromString(text_str);
    for (CsvRow & row : table)
    {
        Painting pt;
        pt = rowToPaint(row);
        this->paints_.push_back(pt);
    }

    return true;
}

bool CsvStorage::close()
{
    string file_name = this->dir_name_;
    ofstream file;
    file.open(file_name, ios::out);
    if (file.fail())
    {
        return false;
    }
    CsvTable mus_table;
    for (Museum & m : this->museums_)
    {
        CsvRow row = MuseumToRow(m);
        mus_table.push_back(row);
    }
    file << Csv::createStringFromTable(mus_table);
    file.close();

    return true;
}

bool CsvStorage::closeExtra()
{
    string file_name = this->dir_name_;
    ofstream file;
    file.open(file_name, ios::out);
    if (file.fail())
    {
        return false;
    }
    CsvTable paint_table;
    for (Painting & p : this->paints_)
    {
        CsvRow row = PaintingToRow(p);
        paint_table.push_back(row);
    }
    file << Csv::createStringFromTable(paint_table);
    file.close();
    return true;
}

void CsvStorage::deleteAll()
{
    this->museums_.clear();
}

int CsvStorage::size()
{
    return this->museums_.size();
}

int CsvStorage::size(int user_id)
{
    return 0;
}

vector<Museum> CsvStorage::getAllMuseums(void)
{
    return this->museums_;
}

optional<Museum> CsvStorage::getMuseumById(int museum_id)
{
    int index;
    for (Museum & mus : this->museums_)
    {
        index = mus.id_;
        if (index == museum_id)
        {
            return mus;
        }
    }
    return nullopt;
}

bool CsvStorage::updateMuseum(const Museum & museum)
{
    auto museums = getAllMuseums();
    for (size_t i = 0; i < museums.size(); i++)
    {
        Museum mus = museums.at(i);
        if (mus.id_ == museum.id_)
        {
            this->museums_.at(i).city_ = museum.city_;
            this->museums_.at(i).name_ = museum.name_;
            this->museums_.at(i).year_ = museum.year_;
            return true;
        }
    }
    return false;
}

bool CsvStorage::removeMuseum(int museum_id)
{
    int index;
    int mus_id = museum_id;
    auto museums = getAllMuseums();
    for (size_t i = 0; i < museums.size(); i++)
    {
        Museum mus = museums.at(i);
        if (mus.id_ == mus_id)
        {
            index = i;
            break;
        }
        if (i == museums.size() - 1)
            return false;
    }
    this->museums_.erase(this->museums_.begin() + index);
    return true;
}

int CsvStorage::insertMuseum(const Museum & museum)
{
    int index = idGenerator::id_get();
    Museum new_mus = museum;
    new_mus.id_ = index;
    this->museums_.push_back(new_mus);
    return index;
}

vector<Painting> CsvStorage::getAllPaintings(void)
{
    return this->paints_;
}

int CsvStorage::insertPainting(const Painting & paint)
{
    int index = idGenerator::id_get();
    Painting new_paint = paint;
    new_paint.id_ = index;
    this->paints_.push_back(new_paint);
    return index;
}

//-------------------------------------------------------------------//

int CsvStorage::insertMuseum(const Museum & museum, int user_id)
{
    return 1;
}

Museum CsvStorage::getMuseumFromQuery(const QSqlQuery & query)
{
    Museum m;
    return m;
}

int CsvStorage::insertPainting(const Painting & painting, int user_id)
{
    return 1;
}

int CsvStorage::removePainting(int painting_id)
{
    return 1;
}

bool CsvStorage::updatePainting(const Painting & painting)
{
    return true;
}

vector<Painting> CsvStorage::getAllUserPaintings(int user_id)
{
    vector<Painting> p;
    return p;
}

vector<Museum> CsvStorage::getAllUserMuseums(int user_id)
{
    vector<Museum>mus;
    return mus;
}

optional<User> CsvStorage::getUserAuth(string & username, string & password)
{
    return nullopt;
}

vector<Painting> CsvStorage::getAllMuseumPaintings(int museum_id, int user_id)
{
    vector<Painting> paints;
    return paints;
}

vector<Painting> CsvStorage::getMuseumPaintings(int museum_id, int user_id, int page_size, int page_number)
{
    vector<Painting> p;
    return p;
}

bool CsvStorage::insertMuseumPainting(int musuem_id, int painting_id, int user_id)
{
    return true;
}

bool CsvStorage::removeMuseumsPainting(int museum_id, int painting_id, int user_id)
{
    return true;
}

bool CsvStorage::removeByMuseumId(int museum_id, int user_id)
{
    return true;
}

bool CsvStorage::removeByPaintingId(int paintig_id, int user_id)
{
    return true;
}

int CsvStorage::numberOfElements(int user_id)
{
    return 0;
}

int CsvStorage::numberOfPaintings(int museum_id)
{
    return 1;
}

vector<Museum> CsvStorage::search(QString search_str, int user_id, int page_size, int page_number)
{
    vector<Museum>mus;
    return mus;
}

int CsvStorage::searchedNumber(QString search_str, int user_id)
{
    return 1;
}
