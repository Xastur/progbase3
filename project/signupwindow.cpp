#include "signupwindow.h"
#include "ui_signupwindow.h"

SignUpWindow::SignUpWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SignUpWindow)
{
    ui->setupUi(this);
    db_ = QSqlDatabase::addDatabase("QSQLITE");
    file_path_ = "/home/dima/progbase3/project/data/sqlite/data.sqlite";
}

SignUpWindow::~SignUpWindow()
{
    delete ui;
}

QString hashPasswordSignUpWindow(QString const & pass)
{
   QByteArray pass_ba = pass.toUtf8();
   QByteArray hash_ba = QCryptographicHash::hash(pass_ba, QCryptographicHash::Md5);
   QString pass_hash = QString(hash_ba.toHex());
   return pass_hash;
}

void SignUpWindow::on_button_cancel_clicked()
{
    SignUpWindow::close();
}

void SignUpWindow::on_button_ok_clicked()
{
    QString username = ui->lineEdit_username->text();
    QString password = ui->lineEdit_password->text();
    QString password_confirm = ui->lineEdit_pass_con->text();
    if (username.length() == 0)
    {
        QMessageBox::information(
            this,
            "Error",
            "Username field is empty");
        return;
    }
    if (password.length() == 0 || password != password_confirm)
    {
        qDebug() << "Mistake in password field";
        QMessageBox::information(
            this,
            "Error",
            "Mistake in password field");
        return;
    }
    db_ = QSqlDatabase::addDatabase("QSQLITE");
    db_.setDatabaseName(file_path_);
    if(!db_.open())
    {
       qDebug() << "Can't open database";
       QMessageBox::information(
           this,
           "Error",
           "Can not open database");
       return;
    }
    if  (usernameExists())
    {
        qDebug() << "This username already exists";
        QMessageBox::information(
            this,
            "Oopsie",
            "This username already exists");
        return;
    }
    QString password_hash = hashPasswordSignUpWindow(password);

    QSqlQuery query;
    query.prepare("INSERT INTO users (username, password_hash) VALUES (:username, :password_hash)");
    query.bindValue(":username", username);
    query.bindValue(":password_hash", password_hash);
    if (!query.exec())
    {
        qDebug() << "Can't insert new user." << query.lastError();;
        QMessageBox::information(
            this,
            "Error",
            "Can not insert new user");
        return;
    }
    qDebug() << "New user successfully created";
    QMessageBox::information(
        this,
        "Information",
        "New user successfully created");
    SignUpWindow::close();
}

bool SignUpWindow::usernameExists()
{
    QString username = ui->lineEdit_username->text();
    QSqlQuery query;
    query.prepare("SELECT * FROM users WHERE username = :username");
    query.bindValue(":username", username);
    if (!query.exec())
    {
       qDebug() << "get username error in signupwindow.cpp:" << query.lastError();
       QMessageBox::information(
           this,
           "Error",
           "Get username error");
       return true;
    }
    if (query.next())
    {
        return true;
    }
    return false;
}
