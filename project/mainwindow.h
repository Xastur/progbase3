#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#pragma once

#include <QMainWindow>
#include <QListWidget>
#include <QDebug>
#include <QFileDialog>
#include <QString>
#include <QDialog>
#include <QtGui>
#include <string.h>
#include <fstream>
#include <sstream>
#include <ctype.h>

#include "addwindow.h"
#include "museum.h"
#include "storage.h"
#include "idgenerator.h"
#include "ui_mainwindow.h"
#include "csv_storage.h"
#include "editwindow.h"
#include "sqlite_storage.h"
#include "user.h"
#include "paintingswindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void loadMuseum(QString file_path);
    ~MainWindow();

signals:
    void sendMuseumToEditForm(QListWidgetItem*);

    void sendUserIdToPF(int user_id);

    void sendMuseumIdToPF(int museum_id);

    void removePaintingsOfMuseum(vector<int>, int);

private slots:
    void on_pushButton_4_clicked();

    void on_listWidget_itemClicked(QListWidgetItem * item);

    void on_pushButton_5_clicked();

    void receiveMuseum(Museum museum);

    void receiveUpdMuseum(Museum museum);

    void on_edit_bt_clicked();

    void receiveUser(User user);

    void on_pushButton_paintings_clicked();

    void on_lineEdit_search_textChanged(const QString &arg1);

    void on_pushButton_x_clicked();

    void on_pushButton_prev_clicked();

    void on_pushButton_next_clicked();

    void updateListWidget();

    void on_actionCU_triggered();

    void on_actionExit_triggered();

    void on_actionImport_triggered();

    void on_actionExport_triggered();

private:
    Ui::MainWindow *ui;
    AddWindow * add_window;
    EditWindow * edit_window;
    PaintingsWindow * paintings_window;
    CsvStorage * csv_storage_;

    Storage * storage_ = nullptr;
    QString file_name;
    User user_;

    int page_number_ = 1;
    int last_page_ = 0;
    int page_size_ = 5;
};

#endif // MAINWINDOW_H
