#ifndef EDITPAINTINGWINDOW_H
#define EDITPAINTINGWINDOW_H

#include <QDialog>
#include <QMainWindow>
#include <QListWidget>
#include <QFileDialog>
#include <QMessageBox>

#include "paint.h"

namespace Ui {
class EditPaintingWindow;
}

class EditPaintingWindow : public QDialog
{
    Q_OBJECT

public:
    explicit EditPaintingWindow(QWidget *parent = 0);
    ~EditPaintingWindow();

signals:
    void sendUpdPainting(Painting);

private slots:
    void on_push_button_cancel_clicked();

    void on_push_button_ok_clicked();

    void receivePaintingToEdit(QListWidgetItem*);

    void on_pushButton_pic_path_clicked();

private:
    Ui::EditPaintingWindow *ui;
};

#endif // EDITPAINTINGWINDOW_H
