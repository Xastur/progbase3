#ifndef ADDPAINTINGWINDOW_H
#define ADDPAINTINGWINDOW_H

#include <QDialog>
#include <QFileDialog>
#include <QMessageBox>

#include "paint.h"
#include "allpaintings.h"

namespace Ui {
class AddPaintingWindow;
}

class AddPaintingWindow : public QDialog
{
    Q_OBJECT

public:
    explicit AddPaintingWindow(QWidget *parent = 0);
    ~AddPaintingWindow();

signals:
    void sendPainting(Painting);

    void sendStorageToAP(Storage*);

    void sendExPaintingToPaintingWindow(Painting);

private slots:
    void on_Cancel_bt_clicked();

    void on_OK_bt_clicked();

    void on_pushButton_pic_path_clicked();

    void on_pushButton_ex_painting_clicked();

    void receiveStorage(Storage*);

    void receivePaintingFromAllPaintings(Painting);

private:
    Ui::AddPaintingWindow *ui;
    Storage * storage_;
    AllPaintings * all_paintings_;
};

#endif // ADDPAINTINGWINDOW_H
