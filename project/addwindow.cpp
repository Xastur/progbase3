#include "addwindow.h"
#include "ui_addwindow.h"
#include "museum.h"
#include "mainwindow.h"

AddWindow::AddWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddWindow)
{
    ui->setupUi(this);
    QIcon icon("/home/dima/progbase3/project/data/pictures/arrow.png");
    ui->pushButton_pic_path->setIcon(icon);
}

AddWindow::~AddWindow()
{
    delete ui;
}

void AddWindow::on_Cancel_bt_clicked()
{
    AddWindow::close();
}

void AddWindow::on_OK_bt_clicked()
{
    if (ui->City_lineEdit->text().length() == 0 || ui->Name_lineEdit->text().length() == 0)
    {
        QMessageBox::information(
            this,
            "Error",
            "City or name line is empty");
        return;
    }

    Museum museum;

    museum.city_ = ui->City_lineEdit->text().toStdString();
    museum.name_ = ui->Name_lineEdit->text().toStdString();
    museum.year_ = ui->spinBox_year->text().toInt();
    museum.picture_path = ui->lineEdit_pic_path->text().toStdString();
    emit sendMuseum(museum);

    AddWindow::close();
}

void AddWindow::on_pushButton_pic_path_clicked()
{
    QString filePath = QFileDialog::getOpenFileName(
                this,              // parent
                "Dialog Caption",  // caption
                "../project/data/pictures",                // directory to start with
                "Image Files (*.png *.jpg *.bmp)");  // file name filter
    if (filePath.length() != 0)
        ui->lineEdit_pic_path->setText(filePath);
}
