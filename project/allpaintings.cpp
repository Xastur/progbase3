#include "allpaintings.h"
#include "ui_allpaintings.h"
#include "paintingswindow.h"

AllPaintings::AllPaintings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AllPaintings)
{
    ui->setupUi(this);
}

AllPaintings::~AllPaintings()
{
    delete ui;
}

void AllPaintings::receiveStorage(Storage * storage)
{
    storage_ = storage;
    updateListWidget();
}

void AllPaintings::updateListWidget()
{
    QSqlQuery query;
    query.prepare("SELECT * FROM paintings");
    if (!query.exec())
    {
        qDebug() << query.lastError();
        return;
    }
    ui->listWidget->clear();
    int i = 0;
    while (query.next())
    {
        QListWidget * ListWidget = ui->listWidget;
        QListWidgetItem * qPaintingListItem = new QListWidgetItem();

        Painting p;
        p.author_ = query.value("author").toString().toStdString();
        p.painting_ = query.value("painting").toString().toStdString();
        p.year_ = query.value("year").toInt();
        p.id_ = query.value("id").toInt();
        p.picture_path_ = query.value("picture_path").toString().toStdString();

        QVariant qVariant;
        qVariant.setValue(p);

        QString name;
        name = QString::fromStdString(to_string(i + 1)) + ". " + (query.value("painting").toString());
        qPaintingListItem->setText(name);
        qPaintingListItem->setData(Qt::UserRole, qVariant);

        ListWidget->addItem(qPaintingListItem);
        i++;
    }
}

void AllPaintings::on_pushButton_cancel_clicked()
{
    AllPaintings::close();
}

void AllPaintings::on_pushButton_ok_clicked()
{
    Painting painting;
    painting = ui->listWidget->selectedItems().at(0)->data(Qt::UserRole).value<Painting>();
    emit(sendPaintingToAddPW(painting));
    AllPaintings::close();
}

void AllPaintings::on_listWidget_itemClicked(QListWidgetItem *item)
{    
    QVariant var = item->data(Qt::UserRole);
    Painting painting = var.value<Painting>();

    ui->pushButton_ok->setEnabled(true);
    ui->paintinglabel->setText(QString::fromStdString(painting.painting_));
    ui->authorlabel->setText(QString::fromStdString(painting.author_));
    ui->yearlabel->setText(QString::fromStdString(to_string(painting.year_)));
}
