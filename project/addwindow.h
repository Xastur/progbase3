#ifndef ADDWINDOW_H
#define ADDWINDOW_H

#pragma once

#include <QDialog>
#include <QMainWindow>
#include <QListWidget>
#include <QMessageBox>

#include "museum.h"
#include "storage.h"
#include "sqlite_storage.h"

namespace Ui {
class AddWindow;
}

class AddWindow : public QDialog
{
    Q_OBJECT

public:
    explicit AddWindow(QWidget *parent = 0);
    ~AddWindow();

public slots:
    void on_Cancel_bt_clicked();

signals:
    void sendMuseum(Museum);

private slots:
    void on_OK_bt_clicked();

    void on_pushButton_pic_path_clicked();

private:
    Ui::AddWindow *ui;
};

#endif // ADDWINDOW_H
