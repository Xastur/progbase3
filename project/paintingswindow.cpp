#include "paintingswindow.h"
#include "ui_paintingswindow.h"

PaintingsWindow::PaintingsWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PaintingsWindow)
{
    ui->setupUi(this);

    QPixmap pix("/home/dima/progbase3/project/data/pictures/white.png");
    ui->label_pic->setPixmap(pix);
}

PaintingsWindow::~PaintingsWindow()
{
    delete storage_;
    delete ui;
}

void PaintingsWindow::load(QString file_name)
{
    file_name_ = file_name;
    SqliteStorage * sql_storage = new SqliteStorage(file_name_.toStdString());
    storage_ = sql_storage;

    if (!storage_->open())
    {
        return;
    }

    int number_of_elements = storage_->numberOfPaintings(museum_id_);
    int number_of_pages = number_of_elements / page_size_;
    if ((number_of_elements % page_size_) != 0 || number_of_elements == 0)
        number_of_pages += 1;
    last_page_ = number_of_pages;
    ui->label_page->setText(QString::fromStdString(to_string(page_number_)) + "/" + QString::fromStdString(to_string(last_page_)));

    updateListWidget();

    ui->listWidget->setEnabled(true);
    ui->pushButton_add->setEnabled(true);
}

void PaintingsWindow::on_pushButton_add_clicked()
{
    add_window = new AddPaintingWindow(this);
    add_window->show();
    all_paintings_window_ = new AllPaintings(this);
    connect(add_window, SIGNAL(sendPainting(Painting)), this, SLOT(receivePainting(Painting)));
    connect(this, SIGNAL(sendStorageToAPW(Storage*)), add_window, SLOT(receiveStorage(Storage*)));
    emit(sendStorageToAPW(storage_));

    connect(add_window, SIGNAL(sendExPaintingToPaintingWindow(Painting)), this, SLOT(receivePaintingFromAddPW(Painting)));
}

void PaintingsWindow::on_pushButton_close_clicked()
{
    PaintingsWindow::close();
}

void PaintingsWindow::receiveUserId(int user_id)
{
    user_id_ = user_id;
}

void PaintingsWindow::receiveMuseumId(int museum_id)
{
    museum_id_ = museum_id;
}

void insertPaintingToCreator(int museum_id, int painting_id)
{
    QSqlQuery query;
    query.prepare("INSERT INTO creators (museum_id, painting_id) VALUES (:museum_id, :painting_id)");
    query.bindValue(":museum_id", museum_id);
    query.bindValue(":painting_id", painting_id);
    if (!query.exec())
    {
        qDebug() << query.lastError();
        return;
    }
}

void PaintingsWindow::receivePainting(Painting painting)
{
    int id = storage_->insertPainting(painting, user_id_);
    if (id == 0)
    {
        return;
    }
    if (!storage_->insertMuseumPainting(museum_id_, id, user_id_))
    {
        return;
    }
    insertPaintingToCreator(museum_id_, id);

    int number_of_elements = storage_->numberOfPaintings(museum_id_);
    if ((number_of_elements % page_size_) == 1 && number_of_elements != 1)
    {
        last_page_ += 1;
        ui->label_page->setText(QString::fromStdString(to_string(page_number_)) + "/" + QString::fromStdString(to_string(last_page_)));
    }

    updateListWidget();
}

void PaintingsWindow::receiveUpdPainting(Painting painting)
{
    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();
    QListWidgetItem * item = items.at(0);
    QVariant var = item->data(Qt::UserRole);
    Painting p = var.value<Painting>();
    painting.id_ = p.id_;

    if(!storage_->updatePainting(painting))
    {
        return;
    }

    updateListWidget();
}

void PaintingsWindow::on_pushButton_remove_clicked()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(
        this,
        "On delete",
        "Are you sure?",
        QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::No)
    {
        return;
    }
    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();
    QListWidgetItem * item = items.at(0);
    items = ui->listWidget->selectedItems();
    QVariant var = item->data(Qt::UserRole);
    Painting p = var.value<Painting>();
    int ind = p.id_;

    if (isCreatorOfPainting(museum_id_, ind))
    {
        if(!storage_->removePainting(ind))
        {
            qDebug() << "Can't remove painting in paintingswindow.cpp";
            return;
        }
        else
        {
            if(!storage_->removeByPaintingId(ind, user_id_))
            {
                qDebug() << "Can't remove connection in paintingswindow.cpp";
                return;
            }
        }
    }
    else
    {
        if(!storage_->removeMuseumsPainting(museum_id_, ind, user_id_))
        {
            qDebug() << "Can't remove connection in paintingswindow.cpp";
            return;
        }
    }

    int number_of_elements = storage_->numberOfPaintings(museum_id_);
    if ((number_of_elements % page_size_) == 0)
    {
        if (page_number_ == last_page_ && last_page_ != 1)
            page_number_ -= 1;
        if (last_page_ != 1)
            last_page_ -= 1;
        ui->label_page->setText(QString::fromStdString(to_string(page_number_)) + "/" + QString::fromStdString(to_string(last_page_)));

    }

    updateListWidget();

    ui->pushButton_edit->setEnabled(false);
    ui->pushButton_remove->setEnabled(false);
    ui->label_pic->setEnabled(false);

    ui->authorlabel->setText("-");
    ui->paintinglabel->setText("-");
    ui->yearlabel->setText("-");

    QPixmap pix("/home/dima/progbase3/project/data/pictures/white.png");
    ui->label_pic->setPixmap(pix);
}

void PaintingsWindow::on_listWidget_itemClicked(QListWidgetItem * painting)
{
    ui->pushButton_remove->setEnabled(true);

    QVariant var = painting->data(Qt::UserRole);
    Painting p = var.value<Painting>();

    string year = to_string(p.year_);
    ui->authorlabel->setText(QString::fromStdString(p.author_));
    ui->paintinglabel->setText(QString::fromStdString(p.painting_));
    ui->yearlabel->setText(QString::fromStdString(year));
    ui->pushButton_edit->setEnabled(true);
    ui->label_pic->setEnabled(true);

    if (QFile::exists(QString::fromStdString(p.picture_path_)))
    {
        QPixmap pix(QString::fromStdString(p.picture_path_));
        ui->label_pic->setPixmap(pix);
    }
    else
    {
        QPixmap pix("/home/dima/progbase3/project/data/pictures/white.png");
        ui->label_pic->setPixmap(pix);
    }
}

void PaintingsWindow::on_pushButton_edit_clicked()
{
    edit_paintings_window = new EditPaintingWindow(this);
    edit_paintings_window->show();

    connect(this, SIGNAL(sendPaintingToEditForm(QListWidgetItem*)), edit_paintings_window, SLOT(receivePaintingToEdit(QListWidgetItem*)));
    connect(edit_paintings_window, SIGNAL(sendUpdPainting(Painting)), this, SLOT(receiveUpdPainting(Painting)));

    QListWidgetItem * item = ui->listWidget->selectedItems().at(0);
    emit(sendPaintingToEditForm(item));

    ui->pushButton_edit->setEnabled(false);
    ui->pushButton_remove->setEnabled(false);
}

void PaintingsWindow::receievePaintingsToDelete(vector<int> paintings_id, int museum_id)
{
    for (size_t i = 0; i < paintings_id.size(); i++)
    {
        if (isCreatorOfPainting(museum_id, paintings_id.at(i)))
        {
            if(!storage_->removePainting(paintings_id.at(i)))
            {
                qDebug() << "Can't remove painting in paintingswindow.cpp";
                return;
            }
            else
            {
                if(!storage_->removeByPaintingId(paintings_id.at(i), user_id_))
                {
                    qDebug() << "Can't remove connection in paintingswindow.cpp";
                    return;
                }
            }
        }
        else
        {
            if(!storage_->removeMuseumsPainting(museum_id, paintings_id.at(i), user_id_))
            {
                qDebug() << "Can't remove connection in paintingswindow.cpp";
                return;
            }
        }
//        storage_->removePainting(paintings_id.at(i));
    }
}

void PaintingsWindow::updateListWidget()
{
    ui->listWidget->clear();
    vector<Painting> paintings = storage_->getMuseumPaintings(museum_id_, user_id_, page_size_, page_number_);

    int num = (page_number_ - 1) * page_size_;
    for (size_t i = 0; i < paintings.size(); i++)
    {
        QListWidget * ListWidget = ui->listWidget;
        QListWidgetItem * qMuseumListItem = new QListWidgetItem();

        QVariant qVariant;
        qVariant.setValue(paintings.at(i));

        QString name;
        name = QString::fromStdString(to_string(num + 1)) + ". " + QString::fromStdString(paintings.at(i).painting_);
        qMuseumListItem->setText(name);
        qMuseumListItem->setData(Qt::UserRole, qVariant);

        ListWidget->addItem(qMuseumListItem);
        num++;
    }
}

void PaintingsWindow::receivePaintingFromAddPW(Painting painting)
{
    ui->authorlabel->setText(QString::fromStdString(painting.author_));
    ui->paintinglabel->setText(QString::fromStdString(painting.painting_));
    ui->yearlabel->setText(QString::fromStdString(to_string(painting.year_)));

    storage_->insertMuseumPainting(museum_id_, painting.id_, user_id_);

    int number_of_elements = storage_->numberOfPaintings(museum_id_);
    if ((number_of_elements % page_size_) == 1 && number_of_elements != 1)
    {
        last_page_ += 1;
        ui->label_page->setText(QString::fromStdString(to_string(page_number_)) + "/" + QString::fromStdString(to_string(last_page_)));
    }
    updateListWidget();
}

bool PaintingsWindow::isCreatorOfPainting(int museum_id, int painting_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM creators WHERE museum_id = :museum_id AND painting_id = :painting_id");
    query.bindValue(":museum_id", museum_id);
    query.bindValue(":painting_id", painting_id);
    if (!query.exec())
    {
        qDebug() << query.lastError();
        return false;
    }
    if (query.next())
    {
        if (query.value("museum_id").toInt() == museum_id)
        {
            qDebug() << museum_id;
            return true;
        }
    }
    return false;
}

void PaintingsWindow::on_pushButton_import_clicked()
{
    QString filePath = QFileDialog::getOpenFileName(
                this,              // parent
                "Dialog Caption",  // caption
                "../project/data/csv",                // directory to start with
                "Csv (*.csv)");  // file name filter

    if (filePath.length() == 0)
    {
        return;
    }
    csv_storage_ = new CsvStorage(filePath.toStdString());
    if(!csv_storage_->openExtra())
    {
        qDebug() << "can not open storage";
        QMessageBox::information(
            this,
            "Error",
            "Can not open storage");
        delete csv_storage_;
        return;
    }

    vector<Painting> paintings = csv_storage_->getAllPaintings();
    for (size_t i = 0; i < paintings.size(); i++)
    {
        int id = storage_->insertPainting(paintings.at(i), user_id_);
        insertPaintingToCreator(museum_id_, id);
        storage_->insertMuseumPainting(museum_id_, id, user_id_);

        int number_of_elements = storage_->numberOfPaintings(museum_id_);
        if ((number_of_elements % page_size_) == 1 && number_of_elements != 1)
        {
            last_page_ += 1;
            ui->label_page->setText(QString::fromStdString(to_string(page_number_)) + "/" + QString::fromStdString(to_string(last_page_)));
        }
    }
    updateListWidget();
    delete csv_storage_;
}

void PaintingsWindow::on_pushButton_export_clicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::Directory);
    QString current_dir = QDir::currentPath();
    QString default_name = "new";
    QString folder_path = dialog.getSaveFileName(
        this,
        "Select Folder To Save CSV File",
        current_dir + "/" + default_name + ".csv",
        "Folders");

    if (folder_path.length() == 0)
    {
        return;
    }
    csv_storage_ = new CsvStorage(folder_path.toStdString());
    vector<Painting> vec_p = storage_->getAllMuseumPaintings(museum_id_, user_id_);
    for (size_t i = 0; i < vec_p.size(); i++)
    {
        csv_storage_->insertPainting(vec_p.at(i));
    }
    if (!csv_storage_->closeExtra())
    {
        qDebug() << "Can not close csv_storage";
        QMessageBox::information(
            this,
            "Error",
            "Can not close storage");
    }
    delete csv_storage_;
}

void PaintingsWindow::on_pushButton_prev_clicked()
{
    ui->pushButton_edit->setEnabled(false);
    ui->pushButton_remove->setEnabled(false);
    if (storage_->numberOfPaintings(museum_id_) == 0)
        return;
    if (page_number_ != 1)
        page_number_ -= 1;
    else
        return;
    ui->label_page->setText(QString::fromStdString(to_string(page_number_)) + "/" + QString::fromStdString(to_string(last_page_)));

    updateListWidget();
}

void PaintingsWindow::on_pushButton_next_clicked()
{
    ui->pushButton_edit->setEnabled(false);
    ui->pushButton_remove->setEnabled(false);
    if (storage_->numberOfPaintings(museum_id_) == 0)
        return;
    if (page_number_ != last_page_)
        page_number_ += 1;
    else
        return;
    ui->label_page->setText(QString::fromStdString(to_string(page_number_)) + "/" + QString::fromStdString(to_string(last_page_)));
    updateListWidget();
}
