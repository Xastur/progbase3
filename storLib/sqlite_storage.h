#ifndef SQLITE_STORAGE_H
#define SQLITE_STORAGE_H

#pragma once

#include <QtSql>
#include <QVariant>

#include "storage.h"
#include "museum.h"
#include "paint.h"

class SqliteStorage : public Storage
{
    QSqlDatabase db_;
    string dir_name_;

public:
    SqliteStorage(const string & dir_name);

    bool open();
    bool close();
    int size();
    int size(int user_id);

    // museums
    vector<Museum> getAllUserMuseums(int user_id);
    vector<Museum> getAllMuseums(void);
    optional<Museum> getMuseumById(int museum_id);
    bool updateMuseum(const Museum & museum);
    bool removeMuseum(int museum_id);
    int insertMuseum(const Museum & museum);
    int insertMuseum(const Museum & museum, int user_id);
    Museum getMuseumFromQuery(const QSqlQuery & query);

    // paintings
    optional<Painting> getPaintingById(int paint_id);
    int insertPainting(const Painting & painting, int user_id);
    int removePainting(int painting_id);
    bool updatePainting(const Painting & painting);
    vector<Painting> getAllUserPaintings(int user_id);
    vector<Painting> getMuseumPaintings(int museum_id, int user_id, int page_size, int page_number);

    // users
    optional<User> getUserAuth(string & username, string & password);

    // links
    vector<Painting> getAllMuseumPaintings(int museums_id, int user_id);
    bool insertMuseumPainting(int musuem_id, int painting_id, int user_id);
    bool removeMuseumsPainting(int museum_id, int painting_id, int user_id);
    bool removeByMuseumId(int museum_id, int user_id);
    bool removeByPaintingId(int paintig_id, int user_id);

    //else
    int numberOfElements(int user_id);
    int numberOfPaintings(int museum_id);
    vector<Museum> search(QString search_str, int user_id, int page_size, int page_number);
    int searchedNumber(QString search_str, int user_id);
};

#endif // SQLITE_STORAGE_H
