#include "sqlite_storage.h"

SqliteStorage::SqliteStorage(const string & dir_name) : dir_name_(dir_name)
{
    db_ = QSqlDatabase::addDatabase("QSQLITE");
}

bool SqliteStorage::open()
{
    QString path = QString::fromStdString(this->dir_name_);
    db_.setDatabaseName(path);
    bool connected = db_.open();
    if (!connected)
    {
      return false;
    }
    return true;
}

int SqliteStorage::size()
{
    return 0;
}

int SqliteStorage::size(int user_id)
{
    vector<Museum> museums = getAllUserMuseums(user_id);
    return museums.size();
}


bool SqliteStorage::close()
{
    db_.close();
    return true;
}

// museums

vector<Museum> SqliteStorage::getAllMuseums(void)
{
    vector <Museum> museums;
    QSqlQuery query("SELECT * FROM museums");
    while (query.next())
    {
        Museum m = getMuseumFromQuery(query);
        museums.push_back(m);
    }
    return museums;
}

optional<Museum> SqliteStorage::getMuseumById(int museum_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM museums WHERE id = :id");
    query.bindValue(":id", museum_id);
    if (!query.exec())
    {
       qDebug() << "get museum error:" << query.lastError();
       return nullopt;
    }
    if (query.next())
    {
        Museum m = getMuseumFromQuery(query);
        return m;
    }
    return nullopt;
}

bool SqliteStorage::updateMuseum(const Museum & museum)
{
    QSqlQuery query;
    query.prepare("UPDATE museums SET name = :name, city = :city, year = :year, picture_path = :picture_path WHERE id = :id");
    query.bindValue(":name", QString::fromStdString(museum.name_));
    query.bindValue(":city", QString::fromStdString(museum.city_));
    query.bindValue(":year", museum.year_);
    query.bindValue(":id", museum.id_);
    query.bindValue(":picture_path", QString::fromStdString(museum.picture_path));

    if (!query.exec())
    {
        qDebug() << query.lastError();
        return false;
    }

    return true;
}

bool SqliteStorage::removeMuseum(int museum_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM museums WHERE id = :id");
    query.bindValue(":id", museum_id);
    if (!query.exec()){
        qDebug() << query.lastError();
        return false;
    }
    if (query.numRowsAffected() == 0)
    {
        return false;
    }
    return true;
}

int SqliteStorage::insertMuseum(const Museum & museum)
{
    return 0;
}

int SqliteStorage::insertMuseum(const Museum &museum, int user_id)
{
    QSqlQuery query;
    query.prepare("INSERT INTO museums (name, city, year, user_id, picture_path) VALUES (:name, :city, :year, :user_id, :picture_path)");
    query.bindValue(":name", QString::fromStdString(museum.name_));
    query.bindValue(":city", QString::fromStdString(museum.city_));
    query.bindValue(":year", museum.year_);
    query.bindValue(":user_id", user_id);
    query.bindValue(":picture_path", QString::fromStdString(museum.picture_path));

    if (!query.exec())
    {
        qDebug() << query.lastError();
        return 0;
    }
    QVariant var = query.lastInsertId();
    return var.toInt();
}

Museum SqliteStorage::getMuseumFromQuery(const QSqlQuery & query)
{
    int id = query.value("id").toInt();
    string name = query.value("name").toString().toStdString();
    string city = query.value("city").toString().toStdString();
    string pic_path = query.value("picture_path").toString().toStdString();
    int year = query.value("year").toInt();
    Museum m;
    m.id_ = id;
    m.name_ = name;
    m.city_ = city;
    m.picture_path = pic_path;
    m.year_ = year;
    return m;
}

// paintings

Painting getPaintingFromQuery(const QSqlQuery & query)
{
    int id = query.value("id").toInt();
    string painting = query.value("painting").toString().toStdString();
    string author = query.value("author").toString().toStdString();
    string picture_path = query.value("picture_path").toString().toStdString();
    int year = query.value("year").toInt();
    Painting p;
    p.id_ = id;
    p.author_ = author;
    p.painting_ = painting;
    p.picture_path_ = picture_path;
    p.year_ = year;
    return p;
}

optional<Painting> SqliteStorage::getPaintingById(int paint_id)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM paintings WHERE id = :id");
    query.bindValue(":id", paint_id);
    if (!query.exec())
    {
       qDebug() << query.lastError();
       return nullopt;
    }
    if (query.next())
    {
        Painting p = getPaintingFromQuery(query);
        return p;
    }
    return nullopt;
}

int SqliteStorage::insertPainting(const Painting & painting, int user_id)
{
    QSqlQuery query;
    query.prepare("INSERT INTO paintings (author, painting, year, user_id, picture_path) VALUES (:author, :painting, :year, :user_id, :picture_path)");
    query.bindValue(":author", QString::fromStdString(painting.author_));
    query.bindValue(":painting", QString::fromStdString(painting.painting_));
    query.bindValue(":year", painting.year_);
    query.bindValue(":picture_path", QString::fromStdString(painting.picture_path_));
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
        qDebug() << query.lastError();
        return 0;
    }
    QVariant var = query.lastInsertId();
    return var.toInt();
}

int SqliteStorage::removePainting(int painting_id)
{
    QSqlQuery query;

    query.prepare("DELETE FROM paintings WHERE id = :id");
    query.bindValue(":id", painting_id);
    if (!query.exec())
    {
        qDebug() << query.lastError();
        return false;
    }
    if (query.numRowsAffected() == 0)
    {
        return false;
    }
    return true;
}

bool SqliteStorage::updatePainting(const Painting & painting)
{
    QSqlQuery query;
    query.prepare("UPDATE paintings SET year = :year, painting = :painting, author = :author, picture_path = :picture_path WHERE id = :id");
    query.bindValue(":painting", QString::fromStdString(painting.painting_));
    query.bindValue(":author", QString::fromStdString(painting.author_));
    query.bindValue(":year", painting.year_);
    query.bindValue(":id", painting.id_);
    query.bindValue(":picture_path", QString::fromStdString(painting.picture_path_));

    if (!query.exec())
    {
        qDebug() << query.lastError();
        return false;
    }
    return true;
}

vector<Painting> SqliteStorage::getAllUserPaintings(int user_id)
{
    QSqlQuery query;
    vector<Painting> vec_p;
    query.prepare("SELECT * FROM paintings WHERE user_id = :user_id");
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
       qDebug() << query.lastError();
       return vec_p;
    }
    while (query.next())
    {
        Painting p = getPaintingFromQuery(query);
        vec_p.push_back(p);
    }
    return vec_p;
}

vector<Painting> SqliteStorage::getMuseumPaintings(int museum_id, int user_id, int page_size, int page_number)
{
    vector<Painting> vec_paintings;
    QSqlQuery query;
    query.prepare("SELECT * FROM links WHERE museum_id = :museum_id AND user_id = :user_id LIMIT :page_size OFFSET :skipped_items");
    query.bindValue(":page_size", page_size);
    query.bindValue(":skipped_items", (page_number - 1) * page_size);
    query.bindValue(":user_id", user_id);
    query.bindValue(":museum_id", museum_id);
    if (!query.exec())
    {
        qDebug() << "ERROR:" << query.lastError();
        return vec_paintings;
    }
    while(query.next())
    {
        optional<Painting> opt = getPaintingById(query.value("painting_id").toInt());
        if (!opt)
        {
            continue;
        }
        Painting painting = opt.value();
        vec_paintings.push_back(painting);
    }
    return vec_paintings;
}

vector<Museum> SqliteStorage::getAllUserMuseums(int user_id)
{
    QSqlQuery query;
    vector<Museum> vec_mus;
    query.prepare("SELECT * FROM museums WHERE user_id = :user_id");
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
       qDebug() << query.lastError();
       return vec_mus;
    }
    while (query.next())
    {
        Museum mus = getMuseumFromQuery(query);
        vec_mus.push_back(mus);
    }
    return vec_mus;
}//new

QString hashPassword(QString const & pass) {
   QByteArray pass_ba = pass.toUtf8();
   QByteArray hash_ba = QCryptographicHash::hash(pass_ba, QCryptographicHash::Md5);
   QString pass_hash = QString(hash_ba.toHex());
   return pass_hash;
}

// users
optional<User> SqliteStorage::getUserAuth(string & username, string & password)
{
    QSqlQuery query;
    optional<User> opt;
    query.prepare("SELECT * FROM users WHERE username = :username AND password_hash = :password_hash;");
    query.bindValue(":username", QString::fromStdString(username));
    query.bindValue(":password_hash", hashPassword(QString::fromStdString(password)));
    if (!query.exec())
    {
        return nullopt;
    }
    if (query.next())
    {
        User user;
        user.id = query.value("id").toInt();
        user.username = username;
        user.password_hash = hashPassword(QString::fromStdString(password)).toStdString();
        opt = user;
        return opt;
    }
    return nullopt;
}//new

// links
vector<Painting> SqliteStorage::getAllMuseumPaintings(int museum_id, int user_id)
{
    vector<Painting> vec_paintings;
    QSqlQuery query;
    query.prepare("SELECT * FROM links WHERE museum_id = :museum_id AND user_id = :user_id;");
    query.bindValue(":user_id", user_id);
    query.bindValue(":museum_id", museum_id);
    if (!query.exec())
    {
       qDebug() << query.lastError();
       return vec_paintings;
    }
    while (query.next())
    {
        optional<Painting> opt = getPaintingById(query.value("painting_id").toInt());
        if (!opt)
        {
            continue;
        }
        Painting painting = opt.value();
        vec_paintings.push_back(painting);
    }
    return vec_paintings;
}

bool isPresent(int museum_id, int painting_id, int user_id)
{
    QSqlQuery query_test;
    query_test.prepare("SELECT museum_id, painting_id, museum_id FROM links WHERE museum_id = :museum_id AND painting_id = :painting_id AND user_id = :user_id");
    query_test.bindValue(":museum_id", museum_id);
    query_test.bindValue(":painting_id", painting_id);
    query_test.bindValue(":user_id", user_id);
    if (!query_test.exec())
    {
        qDebug() << query_test.lastError();
        return true;
    }
    if (query_test.next())
    {
        if (query_test.value("museum_id").toInt() == museum_id)
            return true;
    }
    return false;
}

bool SqliteStorage::insertMuseumPainting(int museum_id, int painting_id, int user_id)
{
    if (isPresent(museum_id, painting_id, user_id))
    {
        return false;
    }

    QSqlQuery query;
    query.prepare("INSERT INTO links (museum_id, painting_id, user_id) VALUES (:museum_id, :painting_id, :user_id)");
    query.bindValue(":museum_id", museum_id);
    query.bindValue(":painting_id", painting_id);
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
        qDebug() << query.lastError();
        return false;
    }
    return true;
}

bool SqliteStorage::removeMuseumsPainting(int museum_id, int painting_id, int user_id)
{
    if (!isPresent(museum_id, painting_id, user_id))
    {
        return false;
    }
    QSqlQuery query;
    query.prepare("DELETE FROM links WHERE museum_id = :museum_id AND painting_id = :painting_id AND user_id = :user_id;");
    query.bindValue(":museum_id", museum_id);
    query.bindValue(":painting_id", painting_id);
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
        return false;
    }
    return true;
}

bool SqliteStorage::removeByMuseumId(int museum_id, int user_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM links WHERE museum_id = :museum_id AND user_id = :user_id;");
    query.bindValue(":museum_id", museum_id);
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
        return false;
    }
    return true;
}

bool SqliteStorage::removeByPaintingId(int painting_id, int user_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM links WHERE painting_id = :painting_id");
    query.bindValue(":painting_id", painting_id);
    if (!query.exec())
    {
        return false;
    }
    return true;
}

int SqliteStorage::numberOfElements(int user_id)
{
    QSqlQuery query;
    query.prepare("SELECT COUNT(*) FROM museums WHERE user_id = :user_id");
    query.bindValue(":user_id", user_id);
    if (!query.exec())
    {
        qDebug() << "ERROR 2:" << query.lastError();
        return -1;
    }
    while(query.next())
    {
        return query.value("COUNT(*)").toInt();
    }
}

int SqliteStorage::numberOfPaintings(int museum_id)
{
    QSqlQuery query;
    query.prepare("SELECT COUNT(*) FROM links WHERE museum_id = :museum_id");
    query.bindValue(":museum_id", museum_id);
    if (!query.exec())
    {
        qDebug() << "ERROR:" << query.lastError();
        return -1;
    }
    while(query.next())
    {
        return query.value("COUNT(*)").toInt();
    }
}

vector<Museum> SqliteStorage::search(QString search_str, int user_id, int page_size, int page_number)
{
    QSqlQuery query;
    query.prepare("SELECT * FROM museums WHERE user_id = :user_id AND name LIKE ('%' || :search_text || '%') LIMIT :page_size OFFSET :skipped_items");
    query.bindValue(":search_text", search_str);
    query.bindValue(":user_id", user_id);
    query.bindValue(":page_size", page_size);
    query.bindValue(":skipped_items", (page_number - 1) * page_size);
    vector <Museum> vec_mus;
    if (!query.exec())
    {
        qDebug() << "ERROR:" << query.lastError();
        return vec_mus;
    }
    while(query.next())
    {
        Museum museum = getMuseumFromQuery(query);
        vec_mus.push_back(museum);
    }
    return vec_mus;
}

int SqliteStorage::searchedNumber(QString search_str, int user_id)
{
    QSqlQuery query;
    query.prepare("SELECT COUNT(*) FROM museums WHERE user_id = :user_id AND name LIKE ('%' || :search_text || '%')");
    query.bindValue(":user_id", user_id);
    query.bindValue(":search_text", search_str);
    if (!query.exec())
    {
        qDebug() << "ERROR:" << query.lastError();
        return -1;
    }
    while(query.next())
    {
        return query.value("COUNT(*)").toInt();
    }
}
