#include "editpaintingwindow.h"
#include "ui_editpaintingwindow.h"

EditPaintingWindow::EditPaintingWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditPaintingWindow)
{
    ui->setupUi(this);
    QIcon icon("/home/dima/progbase3/project/data/pictures/arrow.png");
    ui->pushButton_pic_path->setIcon(icon);
}

EditPaintingWindow::~EditPaintingWindow()
{
    delete ui;
}

void EditPaintingWindow::on_push_button_cancel_clicked()
{
    EditPaintingWindow::close();
}

void EditPaintingWindow::on_push_button_ok_clicked()
{
    if (ui->lineEdit_new_author->text().length() == 0 || ui->lineEdit_new_painting->text().length() == 0)
    {
        QMessageBox::information(
            this,
            "Error",
            "Author or name line is empty");
        return;
    }
    Painting painting;
    painting.author_ = ui->lineEdit_new_author->text().toStdString();
    painting.painting_ = ui->lineEdit_new_painting->text().toStdString();
    painting.year_ = ui->spinBox_new_year->text().toInt();
    painting.picture_path_ = ui->lineEdit_pic_path->text().toStdString();
    emit sendUpdPainting(painting);

    EditPaintingWindow::close();
}

void EditPaintingWindow::receivePaintingToEdit(QListWidgetItem * item)
{
    QVariant var = item->data(Qt::UserRole);
    Painting p = var.value<Painting>();

    ui->lineEdit_new_author->setText(QString::fromStdString(p.author_));
    ui->lineEdit_new_painting->setText(QString::fromStdString(p.painting_));
    ui->spinBox_new_year->setValue(p.year_);
    ui->lineEdit_pic_path->setText(QString::fromStdString(p.picture_path_));
}

void EditPaintingWindow::on_pushButton_pic_path_clicked()
{
    QString filePath = QFileDialog::getOpenFileName(
                this,              // parent
                "Dialog Caption",  // caption
                "../project/data/pictures",                // directory to start with
                "Image Files (*.png *.jpg *.bmp)");  // file name filter
    ui->lineEdit_pic_path->setText(filePath);
}
